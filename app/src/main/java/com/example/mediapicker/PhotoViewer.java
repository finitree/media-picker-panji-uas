package com.example.mediapicker;

import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

public class PhotoViewer extends AppCompatActivity {
    PhotoView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);
        photoView = findViewById(R.id.photo_viewer);

        Uri uri = Uri.parse(getIntent().getStringExtra("uri"));
        Picasso.get().load(uri).into(photoView);
    }
}