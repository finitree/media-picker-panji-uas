package com.example.mediapicker;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.potyvideo.library.AndExoPlayerView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    MaterialButton btnCaptureImage;
    MaterialButton btnCaptureVideo;
    MaterialCardView cvImageCapture;
    ImageView ivImageCapture;
    AndExoPlayerView videoPlayer;

    Integer IMAGE_REQUEST_CODE = 1;
    Integer VIDEO_REQUEST_CODE = 2;

    Uri imageUri;
    Uri videoUri;

    MultiplePermissionsListener dialogMultiplePermissionsListener =
            DialogOnAnyDeniedMultiplePermissionsListener.Builder
                    .withContext(this)
                    .withTitle("Camera & Storage permission")
                    .withMessage("Both camera and Storage permission are needed to continue")
                    .withButtonText("Ok")
                    .build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Dexter.withContext(this).withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).withListener(dialogMultiplePermissionsListener).check();
        initView();
    }

    private void initView() {
        btnCaptureImage = findViewById(R.id.btn_capture_image);
        btnCaptureVideo = findViewById(R.id.btn_capture_video);
        ivImageCapture = findViewById(R.id.iv_image_capture);
        cvImageCapture = findViewById(R.id.cv_image_view);
        videoPlayer = findViewById(R.id.video_player);
        btnCaptureImage.setOnClickListener(view -> captureImage());

        btnCaptureVideo.setOnClickListener(view -> captureVideo());

        cvImageCapture.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, PhotoViewer.class);
            intent.putExtra("uri", imageUri.toString());
            startActivity(intent);
        });
    }

    private void captureImage() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        File file = Utils.getOutputMediaFile(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE);
        imageUri = Uri.fromFile(file);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, IMAGE_REQUEST_CODE);
    }

    private void captureVideo() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        File file = Utils.getOutputMediaFile(MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO);
        videoUri = Uri.fromFile(file);
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
        startActivityForResult(intent, VIDEO_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == IMAGE_REQUEST_CODE) {
                cvImageCapture.setVisibility(View.VISIBLE);
                Picasso.get().load(
                        imageUri
                ).into(ivImageCapture);
            }
            if (requestCode == VIDEO_REQUEST_CODE) {
                videoPlayer.setVisibility(View.VISIBLE);
                videoPlayer.setSource(videoUri.getPath(), new HashMap<>());
            }
        }
    }


}